import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/App';
import { loadFonts } from './js/utils';
import './fonts/pfdindisplaypro-medium.ttf';
import registerServiceWorker from './js/registerServiceWorker';

import './css/index.css';

loadFonts();
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
