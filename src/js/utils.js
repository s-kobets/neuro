import FontFaceObserver from 'fontfaceobserver';

export function loadFonts() {
  const pD = new FontFaceObserver('PF Din Display Pro');
  const pDB = new FontFaceObserver('PF Din Display Pro Bold');
  const pC = new FontFaceObserver('PF Din Text Cond Pro');
  const pCB = new FontFaceObserver('PF Din Text Cond Pro Bold');

  pD.load();
  pDB.load();
  pC.load();
  pCB.load();
}

export function getCookie(name) {
  // eslint no-useless-escape
  const matches = document.cookie.match(
    new RegExp(
      '(?:^|; )' + name.replace('/([.$?*|{}()[]\\/+^])/g', '\\$1') + '=([^;]*)'
    )
  );

  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function setCookie(name, value, options) {
  options = options || {};

  // let expires = options.expires || 0;

  // if (typeof expires == 'number') {
  //   const d = new Date();
  //   d.setTime(d.getTime() + expires * 1000);
  //   expires = options.expires = d;
  // }
  // if (expires.toUTCString) {
  //   options.expires = expires.toUTCString();
  // }

  value = encodeURIComponent(value);

  let updatedCookie = `${name}=${value}`;

  Object.keys(options).forEach(
    propName => (updatedCookie += `${propName}=${options[propName]}`)
  );

  document.cookie = updatedCookie;
}

export function getDataForm(elements) {
  const elementForm = {};
  for (let i = 0; i < elements.length; i += 1) {
    if (elements[i].name) {
      if (elements[i].type !== 'checkbox') {
        elementForm[elements[i].name] = elements[i].value;
      } else {
        elementForm[elements[i].name] = elements[i].checked;
      }
    }
  }
  return elementForm;
}

export function createArray(value, start = 0) {
  let finish = value;
  let result = [];
  let counter = start;
  while (counter !== finish) {
    counter += 1;

    result.push(counter);
  }
  return result;
}
