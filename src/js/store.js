import React from 'react';
import { withRouter } from 'react-router-dom';
const { Provider, Consumer } = React.createContext('store');

const store = {
  user: {}, // email: 'test@test.ru'
  session: {},
  getData: name => store[name],
  setData: (name, data) => {
    store[name] = { ...store[name], ...data };
    return store[name];
  }
};

function ContextProvider(props) {
  return <Provider value={props}>{props.children}</Provider>;
}

const ContextConsumer = Component =>
  withRouter(props => (
    <Consumer {...props}>
      {store => <Component store={store.value} {...props} />}
    </Consumer>
  ));

export { store, ContextConsumer };
export default ContextProvider;
