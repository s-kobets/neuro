import axios from 'axios';
import { getCookie } from './utils';

const serverUrl = 'https://neurostart.herokuapp.com'; //'http://127.0.0.1:8080'

const headerToken = tokenId => ({
  'Access-Control-Allow-Headers': 'x-access-token',
  'x-access-token': tokenId
});

const options = (method, data, url) => {
  const tokenId = getCookie('tokenId');
  const defaultOptions = {
    method,
    headers: {
      'content-type': 'application/json'
    },
    data,
    url
  };
  if (tokenId && (url.includes('/sessions/') || url.includes('/user/'))) {
    return {
      ...defaultOptions,
      headers: {
        ...defaultOptions.headers,
        ...headerToken(tokenId)
      }
    };
  }
  return defaultOptions;
};

export default {
  authenticate(data) {
    return axios(options('POST', data, `${serverUrl}/users/authenticate`));
  },
  create(data) {
    return axios(options('POST', data, `${serverUrl}/users/create`));
  },
  forgot(data) {
    return axios(options('POST', data, `${serverUrl}/users/forgot`));
  },
  sessionsList() {
    const tokenId = getCookie('tokenId');
    return axios.get(`${serverUrl}/sessions/list`, {
      headers: {
        'content-type': 'application/json',
        ...headerToken(tokenId)
      }
    });
  },
  changePassword(data) {
    return axios(options('POST', data, `${serverUrl}/user/change-password`));
  },
  userUpdate(data) {
    return axios(options('POST', data, `${serverUrl}/user/update`));
  },
  userInfo() {
    const tokenId = getCookie('tokenId');
    console.log({
      'content-type': 'application/json',
      ...headerToken(tokenId)
    });
    return axios.get(`${serverUrl}/user/info`, {
      'content-type': 'application/json',
      ...headerToken(tokenId)
    });
  },
  deleteSession(id) {
    return axios(options('POST', {}, `${serverUrl}/sessions/${id}/delete`));
  },
  editSession(id, data) {
    return axios(options('POST', data, `${serverUrl}/sessions/${id}/edit`));
  }
};
