import React, { PureComponent } from 'react';
import { Redirect } from 'react-router';

import { ContextConsumer } from '../store';
import Input from '../components/Input';
import Button, { ButtonLink } from '../components/Button';
import Modal from '../components/Modal';

import ShapeImage from '../../image/shape-20.png';

import { setCookie, getDataForm } from '../utils';
import api from '../api';

class Authorization extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      errors: {
        password: false,
        email: false
      },
      modalValidateIsOpen: false,
      modalRecoveryIsOpen: false,
      forgot: false
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const data = getDataForm(e.currentTarget);
    api
      .authenticate(data)
      .then(resolve => {
        const { success, result, message } = resolve.data;
        if (success) {
          this.props.store.setData('user', { email: data.email });
          setCookie('tokenId', result);
          this.setState({ errors: { ...this.state.errors, password: false } });
        } else {
          if (message.includes('User not found')) {
            this.changeVisibleModal({ modalValidateIsOpen: true });
          }
          if (message.includes('password')) {
            this.setState({ errors: { ...this.state.errors, password: true } });
          }
          console.error(resolve.data);
        }
      })
      .catch(error => console.error(error));
  };

  handleSubmitRecovery = e => {
    e.preventDefault();
    api
      .forgot(getDataForm(e.currentTarget))
      .then(resolve => {
        const { success } = resolve.data;
        if (success) {
          this.changeVisibleModal({ forgot: true });
          this.setState({ errors: { ...this.state.errors, email: false } });
        } else {
          this.setState({ errors: { ...this.state.errors, email: true } });
          console.error(resolve.data);
        }
      })
      .catch(error => console.error(error));
  };

  changeVisibleModal = options => {
    this.setState(options);
  };

  closeValidateModal = () => {
    this.changeVisibleModal({ modalValidateIsOpen: false });
  };

  routingRegistration = () => {
    this.props.changeTab(2);
    this.changeVisibleModal({ modalValidateIsOpen: true });
  };

  openRecoveryModal = () => {
    this.changeVisibleModal({ modalRecoveryIsOpen: true });
  };

  closeRecoveryModal = () => {
    this.changeVisibleModal({ modalRecoveryIsOpen: false });
  };

  render() {
    const { store } = this.props;
    const {
      errors,
      modalValidateIsOpen,
      modalRecoveryIsOpen,
      forgot
    } = this.state;
    return store.getData('user').email ? (
      <Redirect to="/sessions" />
    ) : (
      <div className="auth">
        <p className="auth__title">
          <img src={ShapeImage} alt="облако" />
          <span className="auth__title__text">
            Авторизуйтесь для сохранения сессий в облаке
          </span>
        </p>

        <form className="form form__auth" onSubmit={this.handleSubmit}>
          <Input placeholder="E-mail" type="text" name="email" mb={18} />
          <Input
            placeholder="Пароль"
            type="password"
            name="password"
            mb={18}
            error={{
              active: errors.password,
              position: 'right',
              children: 'Неверный пароль авторизации'
            }}
          />
          <Button type="submit" theme="primary" mb={27}>
            Войти
          </Button>
        </form>

        <ButtonLink onClick={this.openRecoveryModal}>Забыли пароль?</ButtonLink>

        <Modal
          modalIsOpen={modalValidateIsOpen}
          closeModal={this.closeValidateModal}
          className="modal"
        >
          <p className="modal__title">Сбросить пароль</p>
          <p className="modal__description modal__description_recovery">
            Существующий e-mail не обнаружен в системе
          </p>

          <Button theme="primary" mb={18} onClick={this.routingRegistration}>
            Зарегистрироваться
          </Button>
          <Button theme="hollow" onClick={this.closeValidateModal}>
            Попробовать заново
          </Button>
        </Modal>

        <Modal
          modalIsOpen={modalRecoveryIsOpen}
          closeModal={this.closeRecoveryModal}
          className="modal modal_recovery"
        >
          <p className="modal__title">Сбросить пароль</p>

          {forgot ? (
            [
              <p
                className="modal__description modal__description_recovery"
                key="recovery_1"
              >
                Ваш пароль отправлен на почту
              </p>,
              <Button
                theme="primary"
                mb={18}
                onClick={this.closeRecoveryModal}
                key="recovery_2"
              >
                вернуться к авторизации
              </Button>
            ]
          ) : (
            <form
              onSubmit={this.handleSubmitRecovery}
              style={{
                display: 'flex',
                flexDirection: 'column',
                marginTop: '28px'
              }}
            >
              <Input
                placeholder="ваша электронная почта"
                mb={18}
                error={{
                  active: errors.email,
                  position: 'top',
                  children: 'Такого e-mail не существует в системе'
                }}
              />

              <Button type="submit" theme="primary">
                Отправить пароль на почту
              </Button>
            </form>
          )}
        </Modal>
      </div>
    );
  }
}

export default ContextConsumer(Authorization);
