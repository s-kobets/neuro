import React from 'react';

import Button from '../components/Button';

import Arrow from '../../image/shape-27.png';

const Pagination = props => {
  const { current, all } = props.page;
  return (
    <div className="pagination">
      <p />

      <div className="pagination__arrow">
        <Button
          theme="primary"
          padding="12px 14px"
          onClick={() => props.prevPage()}
        >
          <img
            src={Arrow}
            alt="стрелка влево"
            style={{ width: '16px', height: '18px' }}
          />
        </Button>
        <p>
          {current}/{all}
        </p>
        <Button
          theme="primary"
          padding="12px 14px"
          onClick={() => props.nextPage()}
        >
          <img
            src={Arrow}
            alt="стрелка вправо"
            style={{
              width: '16px',
              height: '18px',
              transform: 'rotate(180deg)'
            }}
          />
        </Button>
      </div>

      <p className="font12">{`Сохранено сессий ${current} из ${all}`}</p>
    </div>
  );
};

export default Pagination;
