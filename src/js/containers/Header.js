import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Button, { ButtonLink } from '../components/Button';
import Modal, { ModalSuccess } from '../components/Modal';
import Input from '../components/Input';
import Select from '../components/Select';

import { getDataForm, setCookie, createArray } from '../utils';
import api from '../api';

const Option = styled.div`
  padding: 5px 0;
  cursor: pointer;
`;

class Header extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      modalAbountIsOpen: false,
      modalProfileIsOpen: false,
      modalSuccessIsOpen: false,
      changePassword: false,
      oldpassword: '',
      newpassword: '',
      newpasswordconfirm: '',
      date: props.date || {
        day: '00',
        month: '00',
        year: '1950'
      }
    };
  }

  changeVisibleModal = options => {
    this.setState(options);
  };

  openAbountModal = () => {
    this.changeVisibleModal({ modalAbountIsOpen: true });
  };

  closeAbountModal = () => {
    this.changeVisibleModal({ modalAbountIsOpen: false });
  };

  openProfileModal = () => {
    this.changeVisibleModal({ modalProfileIsOpen: true });
  };

  closeProfileModal = () => {
    this.changeVisibleModal({ modalProfileIsOpen: false });
  };

  closeSuccessModal = () => {
    this.changeVisibleModal({ modalSuccessIsOpen: false });
  };

  onChangeValueDay = e => {
    const { value } = e.currentTarget.dataset;

    this.setState({
      date: {
        ...this.state.date,
        day: value.toString().length === 1 ? `0${value}` : value
      }
    });
  };

  onChangeValueMonth = e => {
    const { value } = e.currentTarget.dataset;

    this.setState({
      date: {
        ...this.state.date,
        month: value.toString().length === 1 ? `0${value}` : value
      }
    });
  };

  onChangeValueYear = e => {
    const { value } = e.currentTarget.dataset;

    this.setState({
      date: { ...this.state.date, year: value }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = getDataForm(e.currentTarget);
    api
      .userUpdate(data)
      .then(resolve => {
        const { success, message } = resolve.data;
        if (success) {
          this.props.updateEmail(data.email);
          this.changeVisibleModal({ modalProfileIsOpen: false });
          this.changeVisibleModal({ modalSuccessIsOpen: true });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));
  };

  handleChangePassword = () => {
    this.setState({
      changePassword: { changePassword: true }
    });
  };

  changePassword = () => {
    const { password, newpassword, newpasswordconfirm } = this.state;

    api
      .changePassword({ password, newpassword, newpasswordconfirm })
      .then(resolve => {
        const { success, result, message } = resolve.data;
        if (success) {
          setCookie('tokenId', result);
          this.setState({
            changePassword: { changePassword: false }
          });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));
  };

  render() {
    const { email } = this.props;
    const {
      modalAbountIsOpen,
      modalProfileIsOpen,
      modalSuccessIsOpen,
      changePassword,
      date
    } = this.state;

    return (
      <div className="header">
        <ButtonLink
          color="#fff"
          size={14}
          onClick={() => this.openAbountModal()}
        >
          О компании
        </ButtonLink>
        <ButtonLink
          color="#fff"
          size={14}
          onClick={() => this.openProfileModal()}
        >
          {email}
        </ButtonLink>

        <Modal
          modalIsOpen={modalAbountIsOpen}
          closeModal={this.closeAbountModal}
          className="modal"
        >
          <p className="modal__title">О компании</p>
          <p className="modal__description modal__description_sessions">
            Идейные соображения высшего порядка, а также начало повседневной
            работы по формированию позиции представляет собой интересный
            эксперимент проверки системы обучения кадров, соответствует насущным
            потребностям. Таким образом реализация намеченных плановых заданий
            влечет за собой процесс внедрения и модернизации системы обучения
            кадров, соответствует насущным потребностям. Товарищи!
          </p>
        </Modal>

        <Modal
          modalIsOpen={modalProfileIsOpen}
          closeModal={this.closeProfileModal}
          className="modal modal_profile"
        >
          <p className="modal__title">настройки профиля</p>
          <form
            onSubmit={this.handleSubmit}
            style={{ display: 'flex', flexDirection: 'column' }}
          >
            <Input defaultValue={email} name="email" mt={47} mb={28} />

            <p className="modal__description modal__description_profile">
              дата рождения
            </p>
            <div className="modal_profile__date">
              <input
                type="date"
                name="birthdate"
                style={{ display: 'none' }}
                value={`${date.year}-${date.month}-${date.day}`}
                onChange={() => {}}
              />
              <Select value={date.day}>
                {() =>
                  createArray(30).map((item, key) => (
                    <Option
                      onClick={this.onChangeValueDay}
                      data-value={item}
                      key={`${key}_key`}
                    >
                      {item.toString().length === 1 ? `0${item}` : item}
                    </Option>
                  ))}
              </Select>

              <Select value={date.month}>
                {() =>
                  createArray(12).map((item, key) => (
                    <Option
                      onClick={this.onChangeValueMonth}
                      data-value={item}
                      key={`${key}_key`}
                    >
                      {item.toString().length === 1 ? `0${item}` : item}
                    </Option>
                  ))}
              </Select>

              <Select value={date.year}>
                {() =>
                  createArray(2018, 1950).map((item, key) => (
                    <Option
                      onClick={this.onChangeValueYear}
                      data-value={item}
                      key={`${key}_key`}
                    >
                      {item}
                    </Option>
                  ))}
              </Select>
            </div>

            {changePassword ? (
              <div>
                <p className="modal__description modal__description_profile">
                  изменить пароль
                </p>
                <div className="modal_profile__password">
                  <Input
                    placeholder="СТАРЫЙ ПАРОЛЬ"
                    name="oldpassword"
                    mb={8}
                    onChange={e =>
                      this.setState({ oldpassword: e.currentTarget.value })}
                  />
                  <Input
                    placeholder="НОВЫЙ ПАРОЛЬ"
                    name="newpassword"
                    mb={8}
                    onChange={e =>
                      this.setState({ newpassword: e.currentTarget.value })}
                  />
                  <Input
                    placeholder="ПОДТВЕРДИТЕ НОВЫЙ ПАРОЛЬ"
                    name="newpasswordconfirm"
                    onChange={e =>
                      this.setState({
                        newpasswordconfirm: e.currentTarget.value
                      })}
                  />
                </div>

                <Button
                  theme="primary"
                  type="button"
                  onClick={this.changePassword}
                  mb={18}
                >
                  СОХРАНИТЬ
                </Button>
              </div>
            ) : (
              <Button
                type="button"
                onClick={this.handleChangePassword}
                mb={27}
                padding="13px 40px 11px"
                style={{ alignSelf: 'center' }}
              >
                изменить пароль
              </Button>
            )}

            {!changePassword && [
              <Button theme="primary" type="submit" mb={18} key="save">
                СОХРАНИТЬ
              </Button>,

              <Button
                theme="hollow"
                type="button"
                onClick={this.closeProfileModal}
                key="exit"
              >
                Выйти из профиля
              </Button>
            ]}
          </form>
        </Modal>

        <ModalSuccess
          modalIsOpen={modalSuccessIsOpen}
          closeModall={this.closeSuccessModal}
        />
      </div>
    );
  }
}

export default Header;
