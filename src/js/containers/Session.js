import React, { PureComponent } from 'react';

import Button from '../components/Button';
import Modal, { ModalSuccess } from '../components/Modal';
import Input from '../components/Input';
import Textarea from '../components/Textarea';
import InputTags from '../components/InputTags';

import ArrowLeft from '../../image/shape-27-blue.png';
import ErrorIcon from '../../image/shape-23.png';
import ShapeImage from '../../image/shape-20.png';
import ListImage from '../../image/shape-22.png';

import api from '../api';
import { getDataForm } from '../utils';

class Session extends PureComponent {
  constructor(props) {
    super(props);

    this.refFile = React.createRef();
    this.state = {
      modalSaveIsOpen: false,
      modalSessionIsOpen: false,
      modalRemoveConfirmIsOpen: false,
      modalRemoveIsOpen: false,
      modalDownlodSessionIsOpen: false,
      modalSuccessIsOpen: false
    };
  }

  changeVisibleModal = options => {
    this.setState(options);
  };

  openModalSave = () => {
    this.changeVisibleModal({ modalSaveIsOpen: true });
  };

  closeModalSave = () => {
    this.changeVisibleModal({ modalSaveIsOpen: false });
  };

  openModalSession = e => {
    this.changeVisibleModal({ modalSessionIsOpen: true });
  };

  closeModalSession = () => {
    this.changeVisibleModal({ modalSessionIsOpen: false });
  };

  openModalRemoveConfirm = () => {
    this.changeVisibleModal({ modalRemoveConfirmIsOpen: true });
  };

  closeModalRemoveConfirm = () => {
    this.changeVisibleModal({ modalRemoveConfirmIsOpen: false });
  };

  closeModalRemove = () => {
    this.changeVisibleModal({ modalRemoveIsOpen: false });
  };

  openDownlodSessionModal = () => {
    this.changeVisibleModal({ modalDownlodSessionIsOpen: true });
  };

  closeDownlodSessionModal = () => {
    this.changeVisibleModal({ modalDownlodSessionIsOpen: false });
  };

  closeSuccessModal = () => {
    this.changeVisibleModal({ modalSuccessIsOpen: false });
  };

  handleYesRemoveConfirm = () => {
    const { id } = this.props.session;
    api
      .deleteSession(id)
      .then(resolve => {
        const { success, message } = resolve.data;
        if (success) {
          this.closeModalRemoveConfirm();
          this.setState({ modalRemoveIsOpen: true });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));
  };

  saveSession = e => {
    e.preventDefault();
    const { id } = this.props.session;
    const data = getDataForm(e.currentTarget);
    api
      .editSession(id, data)
      .then(resolve => {
        const { success, message } = resolve.data;
        if (success) {
          this.closeModalSave();
          this.setState({ modalSuccessIsOpen: true });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));
  };

  handleNoRemoveConfirm = () => {
    this.closeModalRemoveConfirm();
    this.openModalSession();
  };

  handleClickRemove = () => {
    this.closeModalSession();
    this.openModalRemoveConfirm();
  };

  handleClickEdit = () => {
    this.closeModalSession();
    this.openModalSave();
  };

  handleClickOpen = () => {
    this.closeModalSession();
    this.openDownlodSessionModal();
  };

  openFile = () => {
    if (this.refFile.current) {
      this.refFile.current.click();
    }
  };

  render() {
    const { session, page } = this.props;
    const { date, time, sessionName, userName, tags, comment } = session;
    const {
      modalSaveIsOpen,
      modalSessionIsOpen,
      modalRemoveConfirmIsOpen,
      modalRemoveIsOpen,
      modalDownlodSessionIsOpen,
      modalSuccessIsOpen
    } = this.state;

    return (
      <div>
        <div className="session" onClick={this.openModalSession}>
          <div className="session__header">
            <span className="font12">{date}</span>
            <span className="font12">{time}</span>
          </div>
          <p className="session__name">{sessionName}</p>
          <p className="font12">{userName}</p>
        </div>
        <Modal
          modalIsOpen={modalSessionIsOpen}
          closeModal={this.closeModalSession}
          className="modal modal_session-open"
        >
          <p className="modal__title">Выбор сессии</p>
          <p
            className="modal__description modal__description_session"
            onClick={this.closeModalSession}
            style={{ cursor: 'pointer' }}
          >
            <img
              src={ArrowLeft}
              alt="вернуться к списку"
              style={{ width: '7px', marginRight: '7px' }}
            />К списку сессий
          </p>

          <div className="modal__content__session">
            <div className="modal__session__header session__header">
              <span className="font12">{date}</span>
              <span className="font12">{userName}</span>

              <div className="modal__session__header__content">
                <div>
                  <p className="session__name">{sessionName}</p>
                  <p className="font14" style={{ textTransform: 'uppercase' }}>
                    Длительность: {time}
                  </p>
                </div>
                <div className="block__tags" key="block__tags">
                  {tags.map((item, index) => (
                    <span key={`tag-${index}`}>{item}</span>
                  ))}
                </div>
              </div>
            </div>
            <div className="modal__comment__session">
              <p className="modal__title_session">комментарии к записи</p>
              <div className="modal__comment__session__scroll">{comment}</div>

              <div className="modal_session__button">
                <Button
                  type="button"
                  theme="primary"
                  onClick={this.handleClickOpen}
                >
                  открыть
                </Button>
                <Button
                  type="button"
                  theme="primary"
                  onClick={this.handleClickEdit}
                >
                  Редактировать
                </Button>
                <Button
                  type="button"
                  theme="gray"
                  onClick={this.handleClickRemove}
                >
                  удалить
                </Button>
              </div>
            </div>
          </div>
        </Modal>

        <Modal
          modalIsOpen={modalRemoveIsOpen}
          closeModal={this.closeModalRemove}
          className="modal modal_remove"
        >
          <p className="modal__title">сессия удалена</p>

          <Button
            type="button"
            theme="primary"
            mt={44}
            onClick={() => this.closeModalRemove()}
          >
            продолжить
          </Button>
        </Modal>

        <Modal
          modalIsOpen={modalRemoveConfirmIsOpen}
          closeModal={this.closeModalRemoveConfirm}
          className="modal modal_removeConfirm"
        >
          <p className="modal__title">удалить сессию</p>
          <p className="error modal_remove-confirm__description">
            <img
              src={ErrorIcon}
              alt="предупреждение"
              style={{ width: '19px', marginRight: '9px' }}
            />Вы уверены что хотите удалить сессию?
          </p>

          <div className="modal_remove-confirm__button">
            <Button
              type="button"
              theme="primary"
              onClick={this.handleYesRemoveConfirm}
            >
              да
            </Button>
            <Button
              type="button"
              theme="primary"
              onClick={this.handleNoRemoveConfirm}
            >
              нет
            </Button>
          </div>
        </Modal>

        <Modal
          modalIsOpen={modalSaveIsOpen}
          closeModal={this.closeModalSave}
          className="modal modal_session-download"
        >
          <p className="modal__title">сохранить сессию</p>
          <div className="modal__session-header">
            <span>{session.date}</span>
            <span>{session.time}</span>
            <span>{`Сохранено сессий ${page.current} из ${page.all}`}</span>
          </div>

          <form onSubmit={this.saveSession}>
            <Input
              name="sessionName"
              placeholder="имя записи"
              mb={7}
              defaultValue={sessionName}
            />
            <Input
              name="userName"
              placeholder="имя участника"
              mb={7}
              defaultValue={userName}
            />
            <Textarea
              name="comment"
              placeholder="Комментарий к записи"
              defaultValue={comment}
              wrapperProps={{ style: { marginBottom: '27px' } }}
            />
            <p className="modal__description modal__description_session-download">
              теги к записи
            </p>
            <InputTags placeholder="#добавить ваш тег" tags={tags} />

            <div className="modal_save-session__button">
              <Button type="submit" theme="primary" padding="20px 23px 13px">
                СОХРАНИТЬ В ОБЛАКО
              </Button>

              <Button type="submit" theme="hollow" padding="17px 23px 13px">
                СОХРАНИТЬ В ФАЙЛ
              </Button>
            </div>
          </form>
        </Modal>

        <Modal
          modalIsOpen={modalDownlodSessionIsOpen}
          closeModal={this.closeDownlodSessionModal}
          className="modal modal__session"
        >
          <p className="modal__title">Открыть сессию</p>

          <div className="modal__session__button">
            <Button theme="primary" vertical onClick={this.openFile}>
              <img src={ListImage} alt="загрузить файл" />
              <input
                name="open_file"
                type="file"
                ref={this.refFile}
                style={{ display: 'none' }}
              />
              Из сохраненного файла
            </Button>
            <Button theme="primary" vertical>
              <img src={ShapeImage} alt="облако" />
              Из облака
            </Button>
          </div>
        </Modal>

        <ModalSuccess
          modalIsOpen={modalSuccessIsOpen}
          closeModal={this.closeSuccessModal}
        />
      </div>
    );
  }
}

export default Session;
