import React, { PureComponent } from 'react';

import Header from './Header';
import Filtres from './Filtres';
import Session from './Session';
import Pagination from './Pagination';
import { ContextConsumer } from '../store';

import { createArray } from '../utils';

import api from '../api';

class Sessions extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: props.store.getData('user').email,
      birthdate: '',
      session: {
        id: 1,
        date: '15.06.17',
        time: '00:45:32',
        sessionName: 'длинное имя сессии в две строки',
        userName: 'User Name',
        tags: [
          '#Прослушивание музыки',
          '#просмотр кино',
          '#копьютерные игры',
          '#спорт',
          '#ваш тег к записи'
        ],
        comment: `Сервис с помощью технологий искусственного интеллекта создает уникальный фирменный стиль для компании. Пользователю необходимо задать несколько ключевых слов, описывающих бренд на английском языке, и Brandmark создаст логотип, цветовую схему и шрифты.`
      },
      page: {
        current: 6,
        all: 100
      }
    };
  }

  componentDidMount() {
    api
      .sessionsList()
      .then(resolve => {
        const { success, result, message } = resolve.data;
        if (success) {
          const { birthdate, email } = result.user;
          console.info(birthdate);
          this.setState({
            birthdate,
            email
            // date: {
            //   day: '15',
            //   month: '03',
            //   year: '1985'
            // }
          });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));

    api
      .userInfo()
      .then(resolve => {
        const { success, result, message } = resolve.data;
        if (success) {
          const { birthdate, email } = result.user;
          console.info(birthdate);
          this.setState({
            birthdate,
            email
            // date: {
            //   day: '15',
            //   month: '03',
            //   year: '1985'
            // }
          });
        } else {
          console.error(message);
        }
      })
      .catch(error => console.error(error));
  }

  updateEmail = email => {
    this.props.store.setData('user', { email });
    this.setState({
      email
    });
  };

  // next
  increasePage = () => {
    const { current, all } = this.state.page;
    if (current < all) {
      this.setState({
        page: Object.assign({}, this.state.page, {
          current: this.state.page.current + 1
        })
      });
    }
  };

  // prev
  reducePage = () => {
    const { current } = this.state.page;
    if (current > 1) {
      this.setState({
        page: Object.assign({}, this.state.page, {
          current: this.state.page.current - 1
        })
      });
    }
  };

  render() {
    const { email, birthdate, session, page } = this.state;

    return (
      <div className="container sessions">
        <Header
          email={email}
          updateEmail={this.updateEmail}
          birthdate={birthdate}
        />
        <p className="h1 h1__session">NeuroStart</p>
        <p className="h3 h3__session">Выберите сессию из Списка</p>

        <div className="sessions__content">
          <Filtres session={session} page={page} />

          <div className="sessions__list">
            {createArray(6).map((item, index) => (
              <Session session={session} page={page} key={`session_${index}`} />
            ))}
          </div>

          <Pagination
            page={page}
            nextPage={this.increasePage}
            prevPage={this.reducePage}
          />
        </div>
      </div>
    );
  }
}

export default ContextConsumer(Sessions);
