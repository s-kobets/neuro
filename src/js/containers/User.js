import React, { PureComponent } from 'react';

import Tab from '../components/Tab';
import Authorization from './Authorization';
import Registration from './Registration';
// import Link from '../components/Link';

class User extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { tab: 1 };
  }

  changeTab = value => {
    this.setState({ tab: value });
  };

  render() {
    const { tab } = this.state;

    return (
      <div className="container container_user">
        <div className="user__header">
          <p className="h1 h1_user">NeuroStart</p>
        </div>

        <div className="user__tabs">
          <Tab active={tab === 1} onClick={this.changeTab.bind(this, 1)}>
            Авторизация
          </Tab>
          <Tab active={tab === 2} onClick={this.changeTab.bind(this, 2)}>
            Регистрация
          </Tab>
        </div>

        <div className="user__tabs__content">
          {tab === 1 ? (
            <Authorization changeTab={this.changeTab} />
          ) : (
            <Registration />
          )}
        </div>

        {/* <div className="user__footer">
            <Link to="/sessions">Войти без авторизации</Link>
          </div> */}
      </div>
    );
  }
}

export default User;
