import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Select from '../components/Select';
import Chekbox from '../components/Checkbox';
import Button from '../components/Button';
import Modal from '../components/Modal';
import Input from '../components/Input';
import Textarea from '../components/Textarea';
import InputTags from '../components/InputTags';

const FILTERS = ['#просмотр кино', '#копьютерные игры', '#спорт'];

const Option = styled.div`
  padding: 5px 0;
  cursor: pointer;
`;

class Filters extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        date: '',
        tag: ''
      },
      modalIsOpen: false
    };
  }

  onChangeValueTag = e => {
    const { value } = e.currentTarget.dataset;
    this.setState({
      filters: Object.assign({}, this.state.filters, { tag: value })
    });
  };

  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    const { session, page } = this.props;
    const { filters, modalIsOpen } = this.state;

    return (
      <div className="filters">
        <Select
          value="Отфильтровать по длинне и дате"
          size={14}
          padding="7px 28px 6px 8px;"
          arrowRight={11}
        />

        <Select
          value="Отфильтровать по тегу"
          size={14}
          padding="7px 28px 6px 8px;"
          arrowRight={11}
          overlayProps={{ padding: '0 12px' }}
        >
          {() =>
            FILTERS.map((item, key) => (
              <Option key={`${key}_key`}>
                <Chekbox
                  checked={item === filters.tag}
                  data-value={item}
                  mt={0}
                  mb={0}
                  size={14}
                  onClick={this.onChangeValueTag}
                >
                  {item}
                </Chekbox>
              </Option>
            ))}
        </Select>

        <Button
          theme="primary"
          padding="9px 20px 6px"
          size={14}
          onClick={() => this.openModal()}
        >
          Загрузить сессию
        </Button>

        <Modal
          modalIsOpen={modalIsOpen}
          closeModal={this.closeModal}
          className="modal modal_session-download"
        >
          <p className="modal__title">Загрузить сессию</p>
          <div className="modal__session-header">
            <span>{session.date}</span>
            <span>{session.time}</span>
            <span>{`Сохранено сессий ${page.current} из ${page.all}`}</span>
          </div>

          <form>
            <Input placeholder="имя записи" mb={7} />
            <Input placeholder="имя участника" mb={7} />
            <Textarea
              placeholder="Комментарий к записи"
              wrapperProps={{ style: { marginBottom: '27px' } }}
            />

            <p className="modal__description modal__description_session-download">
              теги к записи
            </p>
            <InputTags placeholder="#добавить ваш тег" tags={session.tags} />

            <Button type="button" theme="hollow" padding="16px 23px 14px">
              Выбрать файл
            </Button>
          </form>
        </Modal>
      </div>
    );
  }
}

export default Filters;
