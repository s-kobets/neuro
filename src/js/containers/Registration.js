import React, { PureComponent } from 'react';

import ShapeImage from '../../image/shape-20.png';
import Input from '../components/Input';
import Button from '../components/Button';
import Checkbox from '../components/Checkbox';

import { ContextConsumer } from '../store';
import { getDataForm } from '../utils';
import api from '../api';

class Registration extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errors: {
        email: false,
        password: false,
        passwordconfirm: false,
        deviceId: false
      }
    };
    console.log(props.store.getData('user'));
  }

  handleSubmit = e => {
    const { store } = this.props;
    e.preventDefault();

    api
      .create(getDataForm(e.currentTarget))
      .then(resolve => {
        const { success, result, message } = resolve.data;
        if (success) {
          store.setData('user', result);
          this.setState({
            errors: {
              email: false,
              password: false,
              passwordconfirm: false,
              deviceId: false
            }
          });
        } else {
          if (message.includes('email')) {
            this.setState({ errors: { ...this.state.errors, email: true } });
          }
          if (message.includes('password')) {
            this.setState({ errors: { ...this.state.errors, password: true } });
          }
          console.error(resolve.data);
        }
      })
      .catch(error => console.log(error));
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="reg">
        <p className="auth__title">
          <img src={ShapeImage} alt="облако" />
          <span className="auth__title__text">
            Регистрация дает возможность хранить ваши сессии в облаке и
            расширить гарантию на устройство
          </span>
        </p>

        <form className="form form__reg" onSubmit={this.handleSubmit}>
          <div style={{ display: 'flex', width: '100%' }}>
            <div style={{ flex: 1, marginRight: '10px' }}>
              <Input
                placeholder="E-mail *"
                type="text"
                name="email"
                mb={20}
                error={{
                  active: errors.email,
                  position: 'top',
                  children: 'Такой e-mail уже существует в системе'
                }}
              />
              <Input
                placeholder="Пароль *"
                type="password"
                name="password"
                mb={20}
                error={{
                  active: errors.password,
                  position: 'top',
                  children: 'Пароль не введен'
                }}
              />
            </div>
            <div style={{ flex: 1, marginLeft: '10px' }}>
              <Input
                placeholder="серийный номер устройства *"
                type="text"
                name="deviceId"
                mb={20}
                error={{
                  active: errors.deviceId,
                  position: 'top',
                  children: 'Не верный серийный номер или он уже введен'
                }}
              />

              <Input
                placeholder="подтвердить Пароль *"
                type="password"
                name="passwordconfirm"
                mb={20}
                error={{
                  active: errors.passwordconfirm,
                  position: 'bottom',
                  children: 'Не верный пароль подтверждения'
                }}
              />
            </div>
          </div>

          <div className="reg__docs">
            Сервис с помощью технологий искусственного интеллекта создает
            уникальный фирменный стиль для компании. Пользователю необходимо
            задать несколько ключевых слов, описывающих бренд на английском
            языке, и Brandmark создаст логотип, цветовую схему и шрифты.
            <div className="docs__scroll">
              <button type="button" className="docs__scroll__arrow__top" />
              <button type="button" className="docs__scroll__arrow__bottom" />
            </div>
          </div>

          <Checkbox name="agreement">
            Я согласен на передачу и обработку персональных данных *
          </Checkbox>

          <Button type="submit" theme="primary" mb={27} width={400}>
            зарегистрироваться
          </Button>
        </form>
      </div>
    );
  }
}

export default ContextConsumer(Registration);
