import React from 'react';
import styled from 'styled-components';
import { LinkButton } from '../components/Link';

import logo from '../../image/logo.png';

const HomeIntro = styled.div`
  margin-top: 59px;
  margin-left: 188px;
  width: 456px;
  color: #fff;
`;

const Home = props => (
  <div className="container container_home">
    <img className="Home__logo" src={logo} alt="logo" />
    <HomeIntro>
      <p className="font21">
        Устройство регистрации нейрофизиологических параметров активности
        человека, программный модуль для регистрации и обработки получаемых
        сигналов, интерфейс пользователя.
      </p>

      <LinkButton mt={53} to="/auth">
        Продолжить
      </LinkButton>
    </HomeIntro>
  </div>
);

export default Home;
