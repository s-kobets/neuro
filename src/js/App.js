import React from 'react';
import { HashRouter, Route, withRouter } from 'react-router-dom';

import Home from './containers/Home';
import User from './containers/User';
import Sessions from './containers/Sessions';

import ContextProvider, { store } from './store';

const ConnectedContextProvider = withRouter(ContextProvider);

const App = () => (
  <HashRouter hashType="noslash">
    <ConnectedContextProvider value={store}>
      <Route exact path="/" component={Home} />
      <Route path="/auth" component={User} />
      <Route path="/sessions" component={Sessions} />
    </ConnectedContextProvider>
  </HashRouter>
);

export default App;
