import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const ButtonStyle = styled(Link)`
  position: relative;
  display: inline-flex;
  flex-direction: ${({ vertical }) => 'column'};
  align-items: ${({ vertical }) => 'center'};
  ${({ mt }) => `margin: ${mt}px 0 0 0`};
  padding: 19px 62px 16px;
  font-size: 14px;
  line-height: normal;
  text-transform: uppercase;
  background: ${({ background }) => (background ? background : '#fff')};
  color: ${({ color }) => (color ? color : '#3c6990')};
  font-family: 'PF Din Text Cond Pro', sans-serif;
  border-radius: 3px;
  text-decoration: none;

  &:hover {
    opacity: 0.8;
  }

  & img {
    margin-bottom: ${({ vertical }) => '19px'};
  }
`;

const LinkStyle = styled(Link)`
  color: #3c6990;
  font-family: 'PF Din Text Cond Pro', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 18px;
  text-transform: uppercase;
  text-decoration: none;

  &:hover {
    opacity: 0.8;
  }
`;

const LinkButton = (props, context) => {
  const { children } = props;
  return <ButtonStyle {...props}>{children}</ButtonStyle>;
};

const LinkDefault = (props, context) => {
  const { children } = props;
  return <LinkStyle {...props}>{children}</LinkStyle>;
};

export { LinkButton };
export default LinkDefault;
