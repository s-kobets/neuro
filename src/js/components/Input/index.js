import React from 'react';
import styled, { css } from 'styled-components';

export const InputStyle = css`
  position: relative;
  padding: 0;
  ${({ mt, mb }) => `margin: ${mt ? mt : 0}px 0 ${mb ? mb : 0}px 0;`};
  width: 100%;
  height: 50px;
  border-radius: 3px;
  border-width: 1px;
  border-style: solid;
  border-color: ${({ error }) =>
    error && error.active ? '#ff253a' : '#cdcdcd'};
  color: ${({ error }) => (error && error.active ? '#ff253a' : '#5d6c78')};
  font-size: 16px;
  font-weight: 500;
  text-align: center;
  text-transform: uppercase;
  font-family: 'PF Din Text Cond Pro', sans-serif;
  outline: none;

  &:focus {
    border-width: 2px;
    border-color: ${({ error }) =>
      error && error.active ? '#ff253a' : '#44c8ff'};
  }
`;

const InputStyled = styled.input`${InputStyle};`;

const positionBlock = position => {
  switch (position) {
    case 'right':
      return `top: 0;
      left: calc(100% - 2px);
      padding: 18px 27px 17px;
      border-top-right-radius: 3px;
      border-bottom-right-radius: 3px;`;
    case 'top':
      return `top: calc(-50% + 2px);
      left: 0;
      width: 100%; 
      padding: 10px 5px;
      border-top-left-radius: 3px;
      border-top-right-radius: 3px;`;
    case 'bottom':
      return `bottom: -10px;
      left: 0;
      width: 100%;
      padding: 10px 5px;
      border-bottom-left-radius: 3px;
      border-bottom-right-radius: 3px;`;
    default:
      return `top: 0; left: 0;`;
  }
};

const positionArrow = position => {
  switch (position) {
    case 'right':
      return `
        left: -14px;
        border-right-color: #ff253a;`;
    case 'top':
      return `
        left: calc(50% - 14px);
        bottom: -14px;
        border-top-color: #ff253a;`;
    case 'bottom':
      return `
        left: calc(50% - 14px);
        top: -14px;
        border-bottom-color: #ff253a;
      `;
    default:
      return `top: 0; left: 0;`;
  }
};
const Error = styled.div`
  position: absolute;
  ${({ position }) => positionBlock(position)};
  font-size: 14px;
  font-weight: 500;
  line-height: 15px;
  letter-spacing: 0.35px;
  color: #ffffff;
  background: #ff253a;
  white-space: nowrap;
  text-overflow: ellipsis;
  z-index: 1;

  &::before {
    content: '';
    position: absolute;
    ${({ position }) => positionArrow(position)};
    display: inline-block;
    border-width: 7px;
    border-style: solid;
  }
`;

const Label = styled.label`
  position: relative;
  display: inline-block;
  width: 100%;
`;

const Input = (props, context) => {
  const { children, error } = props;
  return (
    <Label>
      <InputStyled {...props}>{children}</InputStyled>
      {error && error.active && <Error {...error} />}
    </Label>
  );
};

export default Input;
