import React from 'react';
import styled from 'styled-components';

const themeButton = theme => {
  switch (theme) {
    case 'primary':
      return `
        background: linear-gradient(to top, #15a4fa 0%, #15c6db 100%);
        color: #fff;
        border: none;
      `;
    case 'hollow':
      return `
        background:transparent;
        color: #3c6990;
        border:2px solid #3c6990;
      `;
    case 'input':
      return `
        background: #fff;
        color: #5d6c78;
        border:1px solid #cdcdcd;
      `;
    case 'gray':
      return `
        background: #ebebeb;;
        color: #fe2539;
        border: none;
      `;
    default:
      return `
        background: #fff;
        color: #3c6990;
        border: none;
      `;
  }
};

export const ButtonStyle = styled.button`
  position: relative;
  display: inline-flex;
  flex-direction: ${({ vertical }) => 'column'};
  align-items: ${({ vertical }) => 'center'};
  ${({ mt, mb }) => `margin: ${mt ? mt : 0}px 0 ${mb ? mb : 0}px 0;`};
  padding: ${({ padding }) => (padding ? padding : '19px 62px 16px')};
  width: ${({ width }) => (width ? `${width}px` : 'auto')};
  ${({ theme }) => themeButton(theme)};
  font-size: ${({ size }) => (size ? `${size}px` : '16px')};
  line-height: normal;
  text-transform: uppercase;
  font-family: 'PF Din Text Cond Pro', sans-serif;
  border-radius: 3px;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  & img {
    margin-bottom: ${({ vertical }) => vertical && '19px'};
  }
`;

const ButtonLinkStyle = styled.button`
  border: none;
  background: transparent;
  color: ${({ color }) => (color ? color : '#3c6990')};
  font-family: 'PF Din Text Cond Pro', sans-serif;
  font-size: ${({ size }) => (size ? `${size}px` : '16px')};
  font-weight: 500;
  line-height: ${({ size }) => (size ? `${size}px` : '16px')};
  text-transform: uppercase;
  text-decoration: none;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }
`;

const Button = props => {
  const { children } = props;
  return (
    <ButtonStyle type="button" {...props}>
      {children}
    </ButtonStyle>
  );
};

const ButtonLink = props => {
  const { children } = props;
  return (
    <ButtonLinkStyle type="button" {...props}>
      {children}
    </ButtonLinkStyle>
  );
};

export { ButtonLink };
export default Button;
