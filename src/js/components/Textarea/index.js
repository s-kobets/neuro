import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { InputStyle } from '../Input';

const TextAreaBlock = styled.div`
  position: relative;
  display: flex;

  &::after {
    content: attr(data-counter-letter) '/2000';
    position: absolute;
    bottom: 7px;
    right: 7px;
    display: block;
    font-size: 10px;
    color: #b3bbc1;
    background: #fff;
  }
`;
const TextAreaStyle = styled.textarea`
  ${InputStyle};
  padding: 17px;
  height: 110px;
  font-size: 14px;

  text-align: left;
  resize: none;

  ::placeholder {
    text-align: center;
    text-transform: none;
  }
`;

class TextArea extends PureComponent {
  constructor(props) {
    super(props);
    const value = props.value || props.defaultValue || '';
    this.state = {
      value,
      counterLetter: value.length
    };
  }

  handleChange = e => {
    const { value } = e.currentTarget;
    const length = value.length;
    if (length < 2000) {
      this.setState({ value, counterLetter: length });
    }
  };

  render() {
    const { wrapperProps, defaultValue, ...other } = this.props;
    const { counterLetter, value } = this.state;
    return (
      <TextAreaBlock data-counter-letter={counterLetter} {...wrapperProps}>
        <TextAreaStyle
          {...other}
          row={5}
          value={value}
          onChange={this.handleChange}
        />
      </TextAreaBlock>
    );
  }
}

export default TextArea;
