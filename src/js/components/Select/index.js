import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Button from '../Button';

const ButtonSelect = styled(Button)`
  position: relative;
  padding: ${({ padding }) => (padding ? padding : '16px 33px 15px 23px')};

  &::after {
    content: '';
    position: absolute;
    right: ${({ arrowRight }) => (arrowRight ? `${arrowRight}px` : '18px')};
    top: calc(50% - 4px);
    display: block;
    border-width: 4px;
    border-style: solid;
    border-color: transparent;
    ${({ active }) =>
      active
        ? `border-bottom-color: #5d6c78;border-top-width: 0;`
        : 'border-top-color: #5d6c78;border-bottom-width: 0;'};
  }
`;

const Overlay = styled.div`
  position: absolute;
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
  padding: ${({ padding }) => (padding ? padding : 0)};
  width: 100%;
  height: 95px;
  overflow-y: scroll;
  top: calc(100% - 1px);
  background: #fff;
  color: #5d6c78;
  border: 1px solid #cdcdcd;
  border-top-width: 0;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
  z-index: 2;
`;

const SelectStyle = styled.div`
  position: relative;
  display: inline-flex;
`;

class Select extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  handleChangeVisible = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.handleChangeVisible();
    }
  }

  render() {
    const { selectProps, children, value, overlayProps, ...other } = this.props;
    const { isOpen } = this.state;

    return (
      <SelectStyle {...other}>
        <select {...selectProps} style={{ display: 'none' }} />

        <ButtonSelect
          {...other}
          active={isOpen}
          type="button"
          theme="input"
          onClick={this.handleChangeVisible}
        >
          {value.toString().length === 1 ? `0${value}` : value}
        </ButtonSelect>

        <Overlay {...overlayProps} isOpen={isOpen}>
          {children && children()}
        </Overlay>
      </SelectStyle>
    );
  }
}

export default Select;
