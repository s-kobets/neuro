import React, { PureComponent } from 'react';
import styled from 'styled-components';
import check from '../../../image/shape-17.png';

const BlockStyle = styled.div`
  display: flex;
  align-items: center;
  ${({ mt, mb }) =>
    `margin: ${mt && mt.toString() ? mt : 30}px 0 ${mt && mb.toString()
      ? mb
      : 25}px 0;`};
  font-size: ${({ size }) => (size ? `${size}px` : '16px')};
  font-weight: 500;
  line-height: 16px;
  cursor: pointer;
`;

const StyleCheckbox = styled.span`
  position: relative;
  display: inline-block;
  margin-right: 8px;
  width: 16px;
  height: 16px;
  border-radius: 3px;
  border: 1px solid #a1a1a1;
  background-color: ${({ checked }) =>
    checked ? 'rgba(161, 161, 161, 0.2)' : 'rgba(255, 255, 255, 0.3)'};

  &::before {
    content: '';
    position: absolute;
    top: 2px;
    left: 1px;
    display: ${({ checked }) => (checked ? 'block' : 'none')};
    width: 13px;
    height: 10px;
    background-image: url(${check});
    background-size: cover;
  }
`;

class Checkbox extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.checked !== prevState.checked) {
      return {
        prevState,
        checked: nextProps.checked
      };
    }
    return { prevState };
  }
  constructor(props) {
    super(props);
    this.state = { checked: props.checked || false };
  }

  onClick = e => {
    if (this.props.onClick) {
      this.props.onClick(e, !this.state.checked);
    } else {
      this.setState({ checked: !this.state.checked });
    }
  };

  render() {
    const { children, ...other } = this.props;
    const { checked } = this.state;
    return (
      <BlockStyle {...this.props} onClick={this.onClick}>
        <input
          type="checkbox"
          {...other}
          style={{ display: 'none' }}
          checked={checked}
        />
        <StyleCheckbox checked={checked} />
        {children}
      </BlockStyle>
    );
  }
}

export default Checkbox;
