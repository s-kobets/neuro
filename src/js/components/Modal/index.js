import React from 'react';
import Modal from 'react-modal';

import Button from '../Button';

Modal.defaultStyles.overlay.backgroundColor = 'rgba(18, 42, 62, 0.9)';
Modal.defaultStyles.overlay.zIndex = 3;

const ModalView = props => {
  const { modalIsOpen, closeModal, children } = props;
  return (
    <Modal
      {...props}
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      ariaHideApp={false}
    >
      <button className="modal__close" onClick={closeModal} />

      {children}
    </Modal>
  );
};

const ModalSuccess = props => {
  const { modalIsOpen, closeModal } = props;
  return (
    <ModalView
      modalIsOpen={modalIsOpen}
      closeModal={() => closeModal()}
      className="modal modal_success"
    >
      <p className="modal__title">ваши данные успешно изменены</p>
      <Button
        theme="primary"
        type="button"
        mt={43}
        onClick={() => closeModal()}
      >
        продолжить
      </Button>
    </ModalView>
  );
};

export { ModalSuccess };
export default ModalView;
