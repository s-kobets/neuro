import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { InputStyle } from '../Input';

import addIcon from '../../../image/2-layers-tags.png';

const InputTagsBlock = styled.div`
  position: relative;
  display: flex;
`;
const InputTagsStyle = styled.input`${InputStyle};`;

const AddIcon = styled.div`
  position: absolute;
  right: 10px;
  top: calc(50% - 12px);
  width: 23px;
  height: 23px;
  background-image: url(${addIcon});
  background-size: contain;
  cursor: pointer;
`;

class InputTags extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      tags: props.tags || []
    };
  }

  addTag = () => {
    const {value} = this.state
      this.focus()

    if (/^#/.test(value) && value.length > 1) {
      this.setState({tags: [...this.state.tags, value], value: ''})
    }
  }

  handleChange = (e) => {
    this.setState({value: e.currentTarget.value})
  }

  focus = () => {
    if (this.inputRef) {
      this.inputRef.focus();
    }
  };

  render() {
    const { tags, value } = this.state;
    return [
      <InputTagsBlock {...this.props} key="tags">
        <InputTagsStyle innerRef={(node => this.inputRef = node)} value={value} onChange={this.handleChange} {...this.props} />
        <AddIcon onClick={this.addTag}/>
      </InputTagsBlock>,
      <div className="block__tags" key="block__tags">
        {tags.map((item, index) => <span key={`tag-${index}`}>{item}</span>)}
      </div>
    ];
  }
}

export default InputTags;
