import React from 'react';
import styled from 'styled-components';
import rounded from '../../../image/rounded.png';

export const TabStyle = styled.div`
  position: relative;
  flex: 1 1 auto;
  padding: 22px 0 20px;
  font-size: 21px;
  line-height: 28px;
  color: ${({ active }) => (active ? '#5d6c78' : '#fff')};
  background: ${({ active }) => active && '#fff'};
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  text-align: center;
  text-transform: uppercase;
  cursor: pointer;
  font-family: 'PF Din Text Cond Pro', sans-serif;

  &:hover {
    background: ${({ active }) => !active && 'rgba(255,255,255,0.3)'};

    &::after,
    &::before {
      opacity: ${({ active }) => (active ? 1 : 0.3)};
    }
  }

  &::after,
  &::before {
    content: '';
    position: absolute;
    bottom: -1px;
    display: block;
    width: 6px;
    height: 6px;
    background-image: url(${rounded});
    background-size: cover;
    opacity: ${({ active }) => (active ? 1 : 0)};
  }

  &::after {
    right: -5px;
    transform: scale(-1, 1);
  }

  &::before {
    left: -5px;
  }
`;

const Tab = (props, context) => {
  const { children } = props;
  return <TabStyle {...props}>{children}</TabStyle>;
};

export default Tab;
