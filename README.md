# Neuro frontend

https://yadi.sk/i/uqNF-dUX3UaerL - описание проекта
https://yadi.sk/i/CnIHYjk_3UezCk - макеты
https://neurostart.herokuapp.com/api.html - API


# Project structure

- docs
- public - the page template
    - index.html
    - favicon.ico
- site.conf - nginx config
- src - folder with builded static
    - css
    - js
- build - root for project (going npm)

# Commands
`docker-compose up` - run nginx on port 8080


# Description
web/index.html являтеся загрузочным файлом для SPA. 
Роутинг для SPA следует создавать на основе url хэшей.

Nginx проксирует запросы к API, также можно достучаться до документации по адрессу http://127.0.0.1:8080/api.html

Действия, выполняемые при загрузке SPA:

![picture](/docs/init.png)


# Frontend page routing

- \#auth - страница авторизации/регистрации
- \#sessions - страница списка сессий
- \#profile - профиль пользователя
- \#company - компания


TODO Добавить примеры макетов в docs +
